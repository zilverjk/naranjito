const socket = require("../app");
const uuid = require("uuid/v1");
const conexionDB = require("../Config/database");

let data = {
  uuid: null,
  servidor: "main",
  mensaje: "Hola desde el Servidor!"
};

// Establezco conexión
socket.on("connection", (ws, req) => {
  // console.log(req.connection.remoteAddress);
  // Escucho el frontend
  ws.on("message", message => {
    if (message) {
      let dataCliente = JSON.parse(message);
      let uuidCliente = dataCliente.uuid;

      if (uuidCliente == null) {
        data.uuid = uuid();

        //Enviamos el objeto data con el nuevo uuid;
        ws.send(JSON.stringify(data));
      } else {
        if (dataCliente.contador != 0) {
          // console.log(dataCliente);
          // Si le uuid esta lleno, grabamos en la BS lo que envía el frontend
          conexionDB.query(
            `call sp_tdempr_metr (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
            [
              dataCliente.uuid,
              dataCliente.host,
              dataCliente.pagina,
              dataCliente.controlTag,
              dataCliente.controlId,
              dataCliente.controlName,
              dataCliente.contador,
              dataCliente.time,
              data.servidor
            ],
            err => {
              if (err) throw err;
            }
          );
        }
      }
    }
  });
});
