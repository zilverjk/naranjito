var button = document.getElementsByTagName("button");
var link = document.getElementsByTagName("a");

var segundos = 0;

var dataStatistics = {
  uuid: null,
  host: location.host,
  pagina: location.pathname,
  controlTag: null,
  controlId: null,
  controlName: null,
  contador: 0,
  time: segundos
};

var timmer = function() {
  segundos++;
  dataStatistics.time = segundos;
};

// Cuento los segundos en la apagina y lo guardo en la variable "segundos"
setInterval(timmer, 1000);

// Clicks en Botones
for (var i = 0; i < button.length; i++) {
  button[i].addEventListener("click", function() {
    dataStatistics.controlTag = this.tagName;
    dataStatistics.controlId = this.id;
    dataStatistics.controlName = this.innerHTML;
    dataStatistics.contador = 1;

    if (websocket.readyState != 1)
      return ReconectarWs(JSON.stringify(dataStatistics));

    websocket.send(JSON.stringify(dataStatistics));
  });
}

// Clicks en Links
for (var i = 0; i < link.length; i++) {
  link[i].addEventListener("click", function() {
    dataStatistics.controlTag = this.tagName;
    dataStatistics.controlId = this.id;
    dataStatistics.controlName = this.innerHTML;
    dataStatistics.contador = 1;

    if (websocket.readyState != 1)
      return ReconectarWs(JSON.stringify(dataStatistics));

    websocket.send(JSON.stringify(dataStatistics));
  });
}

function ReconectarWs(data) {
  var websocket = new WebSocket("ws://localhost:5000/medidorws");
  websocket.onopen = function() {
    websocket.send(JSON.stringify(data));
  };
}

// Conexión WebSocket
var websocket = new WebSocket("ws://localhost:5000/medidorws");

// Envio datos
websocket.onopen = function() {
  // Si existe el key co_clie_uuid, setea y envía la variable para que el Servidor lo reconozca
  if (localStorage.getItem("co_clie_uuid")) {
    dataStatistics.uuid = localStorage.getItem("co_clie_uuid");
  }

  if (location.host.length === 0) dataStatistics.host = "localhost";
  if (websocket.readyState != 1) return ReconectarWs(co_clie_uuid);

  websocket.send(JSON.stringify(dataStatistics));
};

// Recibo datos
websocket.onmessage = function(mensaje) {
  try {
    var mensajeServidor = JSON.parse(mensaje.data);
  } catch (error) {
    console.log("Error al paresear los datos del servidor: ", mensaje.data);
    console.log(error);
    return;
  }

  // Valido la existencia del key co_clie_uuid en el localstorage
  if (!localStorage.getItem("co_clie_uuid")) {
    localStorage.setItem("co_clie_uuid", mensajeServidor.uuid);
    dataStatistics.uuid = mensajeServidor.uuid;
  }
};

// websocket.onclose = function() {
//   ReconectarWs(dataStatistics);
// };

// Cierra pagina o browser
window.addEventListener("unload", function() {
  dataStatistics.controlTag = "unload";
  dataStatistics.controlId = "unload";
  dataStatistics.controlName = "unload";
  websocket.send(JSON.stringify(dataStatistics));
});
