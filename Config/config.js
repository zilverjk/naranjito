// Asginación del puerto (3000 para prueba)
process.env.PORT = process.env.PORT || 5000;

// Web Path
process.env.deployPath = process.env.deployPath || "";

// Vencimiento Token (1 día)
process.env.TOKEN_CADUCA = 60 * 60 * 24;

// Secret Token
process.env.SECRET_TOKEN = process.env.SECRET_TOKEN || "naranjito-secret-token";
