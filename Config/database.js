const mysql = require("mysql");
const { dbAuth } = require("./keys");
const { promisify } = require("util"); // Para soportar Promesas

const conexionDB = mysql.createPool(dbAuth);

conexionDB.getConnection((err, connection) => {
  if (err) return err;
  return connection.release();
});

// Convertimos las consultas a Promesas
conexionDB.query = promisify(conexionDB.query);

module.exports = conexionDB;
