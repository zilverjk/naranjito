// var url = "http://192.168.1.127:5001/auth";
var urlAuth = "http://localhost:5001/auth";
var urlEmpresa = "http://localhost:5001/empresa";

var loginData = {
  co_toke: "",
  co_usua: "",
  co_pass: ""
};

var datosUsuario = {
  co_usua: "",
  no_usua: "",
  de_mail: "",
  co_grup: "",
  co_esta: ""
};

var datosEmpresa = {
  co_empr: 0,
  de_host: "",
  fe_inic: "",
  fe_fina: ""
};

if (localStorage.getItem("co_toke")) {
  loginData.co_toke = localStorage.getItem("co_toke");
}

if (loginData.co_toke.length > 0) {
  document.getElementById("mainContainer").style.display = "block";
  document.getElementById("divLogin").style.display = "none";

  document.getElementById("navSesion").innerHTML = "Salir";
  document.getElementById(
    "lblUsuario"
  ).innerHTML = `Bienvenido, ${localStorage.getItem("no_usua")}`;
} else {
  document.getElementById("divLogin").style.display = "block";
  document.getElementById("mainContainer").style.display = "none";
}

function loginUsuario() {
  event.preventDefault();
  var co_usua = document.getElementById("txtUsuario").value;
  var co_pass = document.getElementById("txtContraseña").value;

  loginData.co_usua = co_usua;
  loginData.co_pass = co_pass;

  if (localStorage.getItem("co_toke")) {
    loginData.co_toke = localStorage.getItem("co_toke");
  }

  fetch(urlAuth, {
    method: "POST",
    body: JSON.stringify(loginData)
  })
    .then(res => res.json())
    .then(res => {
      if (res.de_mens.length === 0) {
        localStorage.setItem("co_toke", res.co_toke);
        localStorage.setItem("no_usua", res.no_usua);
        localStorage.setItem("de_mail", res.de_mail);

        document.getElementById("navSesion").innerHTML = "Salir";
        document.getElementById("lblUsuario").innerHTML = `Bienvenido, ${
          res.no_usua
        }`;
        document.getElementById("txtFecha_inicial").valueAsDate = new Date();
        document.getElementById("txtFecha_final").valueAsDate = new Date();

        document.getElementById("mainContainer").style.display = "block";
        document.getElementById("divLogin").style.display = "none";
        document.getElementById("tbMetricas").style.visibility = "hidden";

        llenarCombos(true);
      } else {
        document.getElementById("lblValidacionLogin").innerHTML = res.de_mens;
      }
    })
    .catch(console.error);
}

function llenarCombos(sesion) {
  var cboEmpresa = document.getElementById("cbEmpresa"),
    cboHost = document.getElementById("cbHost"),
    optionEmpresa,
    optionHost;

  if (sesion) {
    fetch(urlEmpresa)
      .then(res => res.json())
      .then(res => {
        if (!res) {
          document.getElementById("btnBuscar").style.display = "none";
          return;
        }

        document.getElementById("btnBuscar").style.display = "block";

        for (var i = 0; i < res.length; i++) {
          optionEmpresa = document.createElement("option");
          optionEmpresa.value = res[i].co_empr;
          optionEmpresa.text = res[i].no_empr;
          cboEmpresa.appendChild(optionEmpresa);
        }

        for (let i = 0; i < res.length; i++) {
          optionHost = document.createElement("option");
          optionHost.value = i;
          optionHost.text = res[i].de_host;
          cboHost.appendChild(optionHost);
        }
      });
  } else {
    cboEmpresa.options.length = 0;
    cboHost.options.length = 0;
  }
}

function visualizarMetricas() {
  let cbEmpresa = document.getElementById("cbEmpresa");
  let cbHost = document.getElementById("cbHost");
  let fe_inic = document.getElementById("txtFecha_inicial").value;
  let fe_fina = document.getElementById("txtFecha_final").value;

  datosEmpresa.co_empr = cbEmpresa.options[cbEmpresa.selectedIndex].value;
  datosEmpresa.no_empr = cbEmpresa.options[cbEmpresa.selectedIndex].text;
  datosEmpresa.de_host = cbHost.options[cbEmpresa.selectedIndex].text;
  datosEmpresa.fe_inic = fe_inic;
  datosEmpresa.fe_fina = fe_fina;

  var tablaMetricas = document.getElementById("tbMetricas");
  fetch(urlEmpresa, {
    method: "POST",
    body: JSON.stringify(datosEmpresa)
  })
    .then(res => res.json())
    .then(res => {
      if (!res) {
        return;
      }

      // Muestro la tabla en el DOM
      document.getElementById("tbMetricas").style.visibility = "visible";

      // Limpiamos la tabla
      for (var j = tablaMetricas.rows.length - 1; j > 0; j--) {
        tablaMetricas.deleteRow(j);
      }

      // console.log(res[0]);
      for (var i = 0; i < res.length; i++) {
        var row = document.createElement("tr");
        row.innerHTML = `<td>${res[i].fe_cone}</td>
        <td>${res[i].no_pagi}</td>
        <td>${res[i].de_metr}</td>
        <td>${res[i].no_metr_clie}</td>
        <td>${res[i].nu_cont_metr}</td>
        <td>${res[i].nu_segu_pagi}</td>`;

        tablaMetricas.appendChild(row);
      }
    })
    .catch(console.error);
}

function cerrarSesion() {
  event.preventDefault();

  document.getElementById("divLogin").style.display = "block";
  document.getElementById("mainContainer").style.display = "none";

  document.getElementById("navSesion").innerHTML = "Login";
  document.getElementById("lblValidacionLogin").innerHTML = "";

  localStorage.removeItem("co_toke");
  localStorage.removeItem("no_usua");
  localStorage.removeItem("de_mail");

  llenarCombos(false);
}
