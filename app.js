require("./Config/config");

const port = process.env.PORT;
// const bodyParser = require("body-parser");
const WebSocket = require("ws");
const fs = require("fs");
const path = require("path");
const http = require("http");
// const https = require("https");
const jwt = require("jsonwebtoken");

const conexionDB = require("./Config/database");
// const { validaAuth } = require("./Middlewares/auth");

const scriptPublic = path.resolve(__dirname, "./Public/statisticsws.js");

// WebSocket Config
const socket = new WebSocket.Server({ port });

// Ruta WebSocket
module.exports = socket;
require("./Routes/medidor_ws");

// Ruta Operaciones DB

let port2 = parseInt(port) + 1;

http
  .createServer((req, res) => {
    // CORS Middleware
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Request-Method", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "*");

    if (req.url === "/medidor") {
      fs.readFile(scriptPublic, (err, data) => {
        if (err) return res.end(err);
        res.writeHead(200, { "Content-Type": "text/javascript" });
        res.end(data);
      });
    } else if (req.url === "/auth") {
      if (req.method === "POST") {
        // validaAuth;

        // Declaro varuable de uso
        let body = "";
        let usuarioData = {
          co_usua: null,
          no_usua: null,
          de_mail: null,
          co_grup: null,
          co_esta: null,
          co_toke: null,
          de_mens: ""
        };

        // Request del body que envía el client
        req.on("data", data => {
          body += data;

          // Convierto el objeto a JSON
          let loginData = JSON.parse(body);

          // Validamos que los campos de usuario o password esten lleneos
          if (
            loginData.co_usua.length === 0 ||
            loginData.co_pass.length === 0
          ) {
            usuarioData.de_mens = "Credenciales incompletas";
            res.writeHead(400); // Mando Bad Request
            res.end(JSON.stringify(usuarioData));
            return;
          }

          // Si el cliente no envía un token
          if (loginData.co_toke.length === 0) {
            conexionDB.query(
              `call sp_tmusua_q01 (?, ?)`,
              [loginData.co_usua, loginData.co_pass],
              (err, dataset) => {
                if (err) {
                  res.writeHead(500);
                  res.end(err);
                  return;
                }

                if (!dataset[0][0]) {
                  usuarioData.de_mens = "Credenciales incorrectas";
                  res.writeHead(404);
                  res.end(JSON.stringify(usuarioData));
                  return;
                }
                // Actualizo el objeto usuarioData con el dataset devuelto por el SP
                usuarioData.co_usua = dataset[0][0].co_usua;
                usuarioData.no_usua = dataset[0][0].no_usua;
                usuarioData.de_mail = dataset[0][0].de_mail;
                usuarioData.co_grup = dataset[0][0].co_grup;
                usuarioData.co_esta = dataset[0][0].co_esta;

                // Generamos el token
                let token = jwt.sign(
                  {
                    co_usua: usuarioData.co_usua
                  },
                  process.env.SECRET_TOKEN,
                  { expiresIn: process.env.TOKEN_CADUCA }
                );

                usuarioData.co_toke = token;

                // Envío la respuesta al Client
                res.writeHead(200);
                res.end(JSON.stringify(usuarioData));
                return;
              }
            );
          } else {
            let token = loginData.co_toke;

            // Validamos el token que el cliente envía
            jwt.verify(token, process.env.SECRET_TOKEN, (err, decoded) => {
              if (err) {
                res.writeHead(401); // Mando Unauthorized
                res.end(err);
                return;
              }

              usuarioData.co_usua = decoded.co_usua;
            });

            if (loginData.co_usua === usuarioData.co_usua) {
              conexionDB.query(
                `call sp_tmusua_q01 (?, ?)`,
                [loginData.co_usua, loginData.co_pass],
                (err, dataset) => {
                  if (err) {
                    res.writeHead(500);
                    res.end(err);
                    return;
                  }
                  // Actualizo el objeto usuarioData con el dataset devuelto por el SP
                  usuarioData.co_usua = dataset[0][0].co_usua;
                  usuarioData.no_usua = dataset[0][0].no_usua;
                  usuarioData.de_mail = dataset[0][0].de_mail;
                  usuarioData.co_grup = dataset[0][0].co_grup;
                  usuarioData.co_esta = dataset[0][0].co_esta;
                  usuarioData.co_toke = token;

                  // res.writeHead(200, headers);
                  res.writeHead(200);
                  res.end(JSON.stringify(usuarioData));
                  return;
                }
              );
            }
          }
        });
      }
    } else if (req.url === "/empresa") {
      if (req.method === "GET") {
        let empresaData = {
          co_empr: null,
          no_empr: null,
          de_host: null
        };

        conexionDB.query(
          `select co_empr, no_empr, de_host from tmempr`,
          (err, dataset) => {
            if (err) {
              res.writeHead(500);
              res.end(err);
              return;
            }

            if (!dataset) {
              res.writeHead(404);
              res.end(JSON.stringify(empresaData));
              return;
            }

            // Envío la respuesta al Client
            res.writeHead(200);
            res.end(JSON.stringify(dataset));
            return;
          }
        );
      } else if (req.method === "POST") {
        let body = "";
        let empresaDetalle = {
          fe_cone: null,
          no_pagi: null,
          de_metr: null,
          no_metr_clie: null,
          nu_cont_metr: null,
          nu_segu_pagi: null
        };

        req.on("data", data => {
          body += data;

          let dataBusqueda = JSON.parse(body);

          conexionDB.query(
            `call sp_tdempr_metr_q01 (?, ?, ?, ?)`,
            [
              dataBusqueda.co_empr,
              dataBusqueda.de_host,
              dataBusqueda.fe_inic,
              dataBusqueda.fe_fina
            ],
            (err, dataset) => {
              if (err) {
                res.writeHead(500);
                res.end(err);
                return;
              }

              if (!dataset[0]) {
                res.writeHead(404);
                res.end(JSON.stringify(empresaDetalle));
                return;
              }

              // Envío la respuesta al Client
              res.writeHead(200);
              res.end(JSON.stringify(dataset[0]));
              return;
            }
          );
        });
      }
    }
  })
  .listen(port2);
