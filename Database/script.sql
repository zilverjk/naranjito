use staticsmanager


create table tmempr
(
    co_empr int not null
    auto_increment,
no_empr varchar
    (100) not null,
de_host varchar
    (200) not null, 
co_usua_crea varchar
    (10) not null,
fe_usua_crea datetime not null,
co_usua_modi varchar
    (10) not null,
fe_usua_modi datetime not null,

primary key
    (co_empr)
);

    select*
    from tmempr;

    insert into tmempr
        (no_empr, de_host, co_usua_crea, fe_usua_crea, co_usua_modi, fe_usua_modi)
    values
        ('Promart', 'www.promart.pe', 'acruz', now(), 'acruz', now());


    drop table tmusua;

    create table tmusua
    (
        co_usua varchar(8) not null,
        co_pass varbinary(50) not null,
        no_usua varchar(100),
        de_mail varchar(200),
        co_grup varchar(8) not null,
        co_usua_crea varchar(8) not null,
        fe_usua_crea datetime not null,
        co_usua_modi varchar(8) not null,
        fe_usua_modi datetime not null,

        primary key(co_usua),
        foreign key (co_grup) references ttgrup_usua (co_grup)
    );

    insert into tmusua
    values
        ('acruz01', aes_encrypt('9921', 'naranjito123'), 'André Cruz', 'acruztalavera@gmail.com', 'admin', 'acruz', now(), 'acruz', now());

    select co_usua, cast(co_pass  as char(50)) as co_pass
    from tmusua;

    create table ttgrup_usua
    (
        co_grup varchar(8),
        de_grup varchar(50),
        co_usua_crea varchar(8) not null,
        fe_usua_crea datetime not null,
        co_usua_modi varchar(8) not null,
        fe_usua_modi datetime not null,

        primary key (co_grup)
    );


    insert into ttgrup_usua
    values
        ('admin', 'Administrador', 'acruz', now(), 'acruz', now());

    -- select*from ttgrup_usua


    drop table ttmetr;

    create table ttmetr
    (
        co_metr char(4) not null,
        de_metr varchar(50) not null,
        co_iden varchar(20) not null,
        co_mone char(3) not null,
        pr_metr decimal(18, 4) not null,
        co_usua_crea varchar(8) not null,
        fe_usua_crea datetime not null,
        co_usua_modi varchar(8) not null,
        fe_usua_modi datetime not null,

        primary key (co_metr)
    );

    insert into ttmetr
    values
        ('0001', 'botones', 'button', 'usd', 0.8, 'acruz', now(), 'acruz', now());
    insert into ttmetr
    values
        ('0002', 'link1', 'a', 'usd', 0.4, 'acruz', now(), 'acruz', now());
    insert into ttmetr
    values
        ('0003', 'link2', 'link', 'usd', 0.6, 'acruz', now(), 'acruz', now());

    select*
    from ttmetr;

    drop table trempr_metr;

    create table trempr_metr
    (
        co_empr int,
        co_metr char(4),
        co_usua_crea varchar(8) not null,
        fe_usua_crea datetime not null,
        co_usua_modi varchar(8) not null,
        fe_usua_modi datetime not null,

        primary key (co_empr, co_metr),
        foreign key (co_empr) references tmempr (co_empr),
        foreign key (co_metr) references ttmetr (co_metr)
    );

    select*
    from trempr_metr;

    drop table tcempr_metr;

    /*create table tcempr_metr (
co_empr int not null,
fe_cone datetime not null, 
de_host varchar(200) not null,
co_clie_uuid varchar(255) not null,
nu_segu_tota int, 
st_empr char(3),
st_host char(3),
nu_lati_clie double,
nu_long_clie double,
co_usua_crea varchar(8) not null,
fe_usua_crea datetime not null,
co_usua_modi varchar(8) not null,
fe_usua_modi datetime not null,

primary key(co_empr, fe_cone, co_clie_uuid) 
);*/

    drop table tdempr_metr;

    create table tdempr_metr
    (
        nu_oper bigint not null
        auto_increment,
co_empr int not null,
fe_cone datetime not null,
de_host varchar
        (200) not null,
co_clie_uuid varchar
        (255) not null,
no_pagi varchar
        (200) not null,
co_metr char
        (4) not null,
de_metr varchar
        (50),
no_metr_clie varchar
        (50),
id_metr_clie varchar
        (20),
ta_metr_clie varchar
        (10),
nu_cont_metr int not null,
nu_segu_pagi int not null,
co_usua_crea varchar
        (8) not null,
fe_usua_crea datetime not null,
co_usua_modi varchar
        (8) not null,
fe_usua_modi datetime not null,

primary key
        (nu_oper)
);


        /*create index idx_tcempr_metr_0001 on tcempr_metr (co_empr, fe_cone, de_host);
create index idx_tcempr_metr_0002 on tcempr_metr (co_empr, fe_cone, nu_lati_clie, nu_long_clie);*/

        create index idx_tdempr_metr_0001 on tdempr_metr (co_empr, fe_cone, co_metr);
        create index idx_tdempr_metr_0002 on tdempr_metr (co_empr, fe_cone, ta_metr_clie);
        create index idx_tdempr_metr_0003 on tdempr_metr (co_empr, fe_cone, co_clie_uuid);

        call sp_tcempr_metr
        (
'18abe9f0-b705-11e9-9a48-f3c5818a7738',
'localhost',
'/C:/Projects/Node/Naranjito/Frontend/index.html',
'button',
'btnTest',
'Hazme Click!',
1,
54,
'main'
)

        select *
        from tdempr_metr

DELIMITER
        $$

        create procedure sp_tcempr_metr (
in isco_clie_uuid varchar
        (255),
in isde_host varchar
        (200),
in isno_pagi varchar
        (200),
in ista_metr_clie varchar
        (10),
in isid_metr_clie varchar
        (20),
in isno_metr_clie varchar
        (50),
in innu_cont int, 
in innu_segu int,
in isco_usua varchar
        (8)
)
        begin

            select @vsco_empr := co_empr
            from tmempr
            where de_host = isde_host;

            select @vsco_metr := co_metr,
                @vsde_metr := de_metr
            from ttmetr
            where co_iden = ista_metr_clie;

            if (select 1
            from tdempr_metr
            where co_empr = @vsco_empr
                and fe_cone = date(now())
                and de_host = isde_host
                and co_clie_uuid = isco_clie_uuid
                and no_pagi = isno_pagi
                and co_metr = @vsco_metr) 
then
            update tdempr_metr
    set nu_cont_metr = nu_cont_metr + innu_cont,
    nu_segu_pagi = innu_segu
    where co_empr = @vsco_empr
                and fe_cone = date(now())
                and de_host = isde_host
                and co_clie_uuid = isco_clie_uuid
                and no_pagi = isno_pagi
                and co_metr = @vsco_metr;
            else
            insert into tdempr_metr
                (co_empr, fe_cone, de_host, co_clie_uuid, no_pagi, co_metr, de_metr, no_metr_clie,
                id_metr_clie, ta_metr_clie, nu_cont_metr, nu_segu_pagi, co_usua_crea, fe_usua_crea, co_usua_modi, fe_usua_modi)
            values
                (@vsco_empr, date(now()), isde_host, isco_clie_uuid, isno_pagi, @vsco_metr, @vsde_metr, isno_metr_clie,
                    isid_metr_clie, ista_metr_clie, innu_cont, innu_segu, isco_usua, now(), isco_usua, now());
        end
        if;


end$$
DELIMITER ;
